const express = require('express');
const mongoose = require('mongoose');
//allows our backend application to be available to our frontend application
//allows us  to control app's cross origin resources sharing settings
const cors = require('cors');
const app = express();

//Connect to our MongoDB database
mongoose.connect('mongodb+srv://admin:admin@zuitt-bootcamp.dxzdo.mongodb.net/Batch127_Booking?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

//Prompts a message in the terminal once the connection is 'open' and we are able to succesfully connect to our database
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));

//allows all resources/origins to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//will used the defined port number for the application whenever an environment variable is available OR will used port 4000 if non is defined
//This syntax will allow flexibility when using application locally or as a hosted application
app.listen(process.env.PORT || 4000, ()=> {
    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
})